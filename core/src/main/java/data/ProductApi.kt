package data

import com.recruitment.domain.ProductDetailResponse
import com.recruitment.domain.ProductListResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ProductApi {

    @GET("/products")
    suspend fun getProductList(
        @Query("page") page: Int,
        @Query("pageSize") pageSize: Int = 5
    ): Response<ProductListResponse>

    @GET("/products/{productId}")
    suspend fun getProductDetails(@Path(value = "productId") productId: Int): Response<ProductDetailResponse>
}