package data

import com.recruitment.domain.ProductDetailResponse
import com.recruitment.domain.ProductListResponse
import retrofit2.Response

interface ProductRepository {
    suspend fun getProductList(page: Int): Response<ProductListResponse>

    suspend fun getProductDetails(productId: Int): Response<ProductDetailResponse>
}