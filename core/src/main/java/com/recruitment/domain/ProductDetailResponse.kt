package com.recruitment.domain

data class ProductDetailResponse(
    val id: Int?,
    val name: String?,
    val description: String?,
    val brand: String?,
    val price: Int?,
    val currency: String?,
    val discountPercentage: Int?,
    val image: String?,
    val stock: Int?
)