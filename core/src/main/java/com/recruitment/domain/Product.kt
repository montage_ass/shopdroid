package com.recruitment.domain

data class Product(
    val id: Int?,
    val name: String?,
    val brand: String?,
    val price: Int?,
    val currency: String?,
    val image: String?,
    val _link: String?,
    val _type: String?,
)