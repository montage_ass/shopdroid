package com.recruitment.domain

data class ErrorResponse(
    val httpStatusCode: Int?,
    val internalErrorCode: String?,
    val userMessage: String?,
    val developerMessage: String?,
    val _type: String?
)