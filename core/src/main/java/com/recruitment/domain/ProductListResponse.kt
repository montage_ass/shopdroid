package com.recruitment.domain

data class ProductListResponse(
    val list: List<Product>?,
    val page: Int?,
    val pageSize: Int?,
    val _link: String?,
    val _type: String?,
    val _previous: String?,
    val _next: String?
)
