package com.recruitment.shopdroid.ui.productdetail

import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.recruitment.shopdroid.R
import com.recruitment.shopdroid.databinding.ProductDetailFragmentBinding
import com.recruitment.shopdroid.utils.loadImage
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProductDetailFragment : Fragment() {
    private val viewModel by viewModels<ProductDetailViewModel>()
    private val args: ProductDetailFragmentArgs by navArgs()

    private lateinit var binding: ProductDetailFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = ProductDetailFragmentBinding.inflate(inflater)
        binding.price.paintFlags = binding.price.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.addToCart.setOnClickListener {
            AlertDialog.Builder(context ?: return@setOnClickListener).apply {
                setMessage(R.string.coming_soon)
                setPositiveButton(android.R.string.ok) {_, _ -> }
                show()
            }
        }

        viewModel.loadProductDetails(args.productId)
        viewModel.productDetail.observe(viewLifecycleOwner, ::onProductDetail)
        viewModel.error.observe(viewLifecycleOwner, ::onError)
    }

    private fun onProductDetail(display: ProductDetailDisplay) {
        binding.display = display
        binding.productImage.loadImage(display.imageUrl ?: return)
    }

    private fun onError(userMessage: String?) {
        binding.error.text = userMessage ?: getString(R.string.generic_error_message)
    }
}