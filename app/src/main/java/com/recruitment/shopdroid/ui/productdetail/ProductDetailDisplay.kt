package com.recruitment.shopdroid.ui.productdetail

data class ProductDetailDisplay(
    val name: String,
    val description: String,
    val brand: String,
    val price: String,
    val discountedPrice: String,
    val isDealOfTheDay: Boolean,
    val imageUrl: String?,
    val isInStock: Boolean
)