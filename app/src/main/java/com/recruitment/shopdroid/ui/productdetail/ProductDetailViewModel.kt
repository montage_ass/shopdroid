package com.recruitment.shopdroid.ui.productdetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.recruitment.domain.ErrorResponse
import com.recruitment.domain.ProductDetailResponse
import com.google.gson.Gson
import com.recruitment.shopdroid.utils.onFailure
import com.recruitment.shopdroid.utils.onSuccess
import dagger.hilt.android.lifecycle.HiltViewModel
import data.ProductRepository
import kotlinx.coroutines.launch
import okhttp3.ResponseBody
import java.util.*
import javax.inject.Inject

@HiltViewModel
class ProductDetailViewModel @Inject constructor(
    private val productRepository: ProductRepository
) : ViewModel() {

    private val _productDetail = MutableLiveData<ProductDetailDisplay>()
    val productDetail: LiveData<ProductDetailDisplay> = _productDetail

    private val _error = MutableLiveData<String?>()
    val error: LiveData<String?> = _error

    fun loadProductDetails(productId: Int) =
        viewModelScope.launch {
            productRepository.getProductDetails(productId)
                .onSuccess { createDisplay(it) }
                .onFailure { createError(it) }
        }

    private fun createDisplay(responseData: ProductDetailResponse) =
        responseData.price?.let { price ->
            val pricePercentage = (100 - (responseData.discountPercentage ?: 0)).toDouble() / 100

            val display = with(responseData) {
                ProductDetailDisplay(
                    name = name?.capitalize(Locale.ROOT).orEmpty(),
                    description = description.orEmpty(),
                    brand = brand.orEmpty(),
                    price = price.toDouble().formatPrice(currency),
                    discountedPrice = (price * pricePercentage).formatPrice(currency),
                    isDealOfTheDay = (discountPercentage ?: 0) > 30,
                    imageUrl = image,
                    isInStock = (stock ?: 0) > 0
                )
            }

            _productDetail.postValue(display)
        } ?: _error.postValue(null)

    private fun Double.formatPrice(currency: String?) =
        "%.2f".format(Locale.US, this) + " " + currency.orEmpty()

    private fun createError(errorBody: ResponseBody) =
        runCatching { Gson().fromJson(errorBody.string(), ErrorResponse::class.java) }
            .getOrNull()
            .let { _error.postValue(it?.userMessage) }
}