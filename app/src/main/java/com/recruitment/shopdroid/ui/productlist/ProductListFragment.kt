package com.recruitment.shopdroid.ui.productlist

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import com.recruitment.shopdroid.databinding.ProductListFragmentBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ProductListFragment : Fragment() {

    companion object {
        private const val favoriteProducts = "favoriteProducts"
    }

    private val viewModel by viewModels<ProductListViewModel>()
    private lateinit var binding: ProductListFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = ProductListFragmentBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = ProductListAdapter({ productId, brand ->
            productId?.let {
                findNavController().navigate(ProductListFragmentDirections.goToProductDetail(it, brand))
            }
        }, ::saveFavorite, ::getIsFavorite)
        binding.retry.setOnClickListener { adapter.retry() }
        binding.productList.layoutManager = GridLayoutManager(context, 2)
        binding.productList.adapter = adapter

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.products.collectLatest {
                adapter.submitData(it)
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            adapter.loadStateFlow.collectLatest { loadStates ->
                binding.loadingIndicator.isVisible = loadStates.refresh is LoadState.Loading
                binding.overlay.isVisible = loadStates.refresh is LoadState.Loading
                binding.error.isVisible = loadStates.refresh is LoadState.Error
            }
        }
    }

    private fun saveFavorite(productId: Int, isFavorite: Boolean) {
        val sharedPrefs = activity?.getPreferences(Context.MODE_PRIVATE)
        val favorites = sharedPrefs?.getStringSet(favoriteProducts, emptySet())?.toMutableSet()
        if (isFavorite) {
            favorites?.add(productId.toString())
        } else {
            favorites?.remove(productId.toString())
        }
        sharedPrefs?.edit()?.apply {
            putStringSet(favoriteProducts, favorites)
            apply()
        }
    }

    private fun getIsFavorite(productId: Int): Boolean =
        activity?.getPreferences(Context.MODE_PRIVATE)
            ?.getStringSet(favoriteProducts, null)?.contains(productId.toString()) ?: false
}