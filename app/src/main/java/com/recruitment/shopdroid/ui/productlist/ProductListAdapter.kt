package com.recruitment.shopdroid.ui.productlist

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.recruitment.domain.Product
import com.recruitment.shopdroid.databinding.ProductListItemBinding
import com.recruitment.shopdroid.utils.loadImage
import com.recruitment.shopdroid.utils.setFavoriteIcon

class ProductListAdapter(
    private val onProductClick: (Int?, String) -> Unit,
    private val markAsFavorite: (Int, Boolean) -> Unit,
    private val getIsFavorite: (Int) -> Boolean
) : PagingDataAdapter<Product, ProductListAdapter.ProductListItemViewHolder>(ProductComparator) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductListItemViewHolder {
        return ProductListItemViewHolder(
            ProductListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ProductListItemViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bindItem(it)
        }
    }

    inner class ProductListItemViewHolder(private val binding: ProductListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindItem(item: Product) = with(binding) {
            item.image?.let {
                productImage.loadImage(it)
                productImage.setOnClickListener { onProductClick.invoke(item.id, item.brand ?: "") }
            }
            productText.text = item.brand

            val isMarkedAsFavorite = item.id?.let { getIsFavorite.invoke(it) } ?: false
            productFavorite.setFavoriteIcon(isMarkedAsFavorite)
            productFavorite.setOnClickListener {
                setIsFavorite(it as ImageView, item)
            }
        }
    }

    private fun setIsFavorite(imageView: ImageView, item: Product) {
        item.id?.let {
            val isFavorite = getIsFavorite.invoke(it)
            imageView.setFavoriteIcon(!isFavorite)
            markAsFavorite.invoke(it, !isFavorite)
        }
    }

    object ProductComparator : DiffUtil.ItemCallback<Product>() {
        override fun areItemsTheSame(oldItem: Product, newItem: Product): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Product, newItem: Product): Boolean {
            return oldItem == newItem
        }
    }
}