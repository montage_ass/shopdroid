package com.recruitment.shopdroid.ui.productlist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.recruitment.shopdroid.framework.ProductsDataSource
import dagger.hilt.android.lifecycle.HiltViewModel
import data.ProductRepository
import javax.inject.Inject

@HiltViewModel
class ProductListViewModel @Inject constructor(
    private val productRepository: ProductRepository
) : ViewModel() {

    val products = Pager(PagingConfig(pageSize = 5)) {
        ProductsDataSource(productRepository)
    }.flow.cachedIn(viewModelScope)
}