package com.recruitment.shopdroid.framework

import android.net.Uri
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.recruitment.domain.Product
import data.ProductRepository

class ProductsDataSource(private val productRepository: ProductRepository) :
    PagingSource<Int, Product>() {

    override fun getRefreshKey(state: PagingState<Int, Product>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Product> {
        val pageNumber = params.key ?: 1

        return try {
            val response = productRepository.getProductList(pageNumber)
            val pagedResponse = response.body()
            val data = pagedResponse?.list

            var previousPageNumber: Int? = null
            if (pagedResponse?._previous != null) {
                val uri = Uri.parse(pagedResponse._previous)
                val nextPageQuery = uri.getQueryParameter("page")
                previousPageNumber = nextPageQuery?.toInt()
            }

            var nextPageNumber: Int? = null
            if (pagedResponse?._next != null) {
                val uri = Uri.parse(pagedResponse._next)
                val nextPageQuery = uri.getQueryParameter("page")
                nextPageNumber = nextPageQuery?.toInt()
            }

            LoadResult.Page(
                data = data.orEmpty(),
                prevKey = previousPageNumber,
                nextKey = nextPageNumber
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }

}