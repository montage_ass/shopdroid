package com.recruitment.shopdroid.framework

import com.recruitment.domain.ProductDetailResponse
import com.recruitment.domain.ProductListResponse
import data.ProductApi
import data.ProductRepository
import retrofit2.Response
import javax.inject.Inject

class ProductRepositoryImpl @Inject constructor(
    private val productApi: ProductApi
) : ProductRepository {

    override suspend fun getProductList(page: Int): Response<ProductListResponse> =
        productApi.getProductList(page)

    override suspend fun getProductDetails(productId: Int): Response<ProductDetailResponse> =
        productApi.getProductDetails(productId)
}