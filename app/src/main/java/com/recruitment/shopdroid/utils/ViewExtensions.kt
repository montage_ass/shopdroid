package com.recruitment.shopdroid.utils

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.recruitment.shopdroid.R

fun ImageView.loadImage(url: String) {
    Glide.with(this)
        .load(url)
        .centerInside()
        .transition(DrawableTransitionOptions.withCrossFade())
        .into(this)
}

fun ImageView.setFavoriteIcon(isFavorite: Boolean) =
    setImageResource(if (isFavorite) R.drawable.favorite else R.drawable.favorite_border)