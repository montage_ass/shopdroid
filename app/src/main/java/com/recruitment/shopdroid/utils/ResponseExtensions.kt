package com.recruitment.shopdroid.utils

import okhttp3.ResponseBody
import retrofit2.Response

inline fun <T> Response<T>.onSuccess(func: (T) -> Unit) = apply {
    if (isSuccessful) {
        body()?.let { func.invoke(it) }
    }
}

inline fun <T> Response<T>.onFailure(func: (ResponseBody) -> Unit) = apply {
    if (!isSuccessful) {
        errorBody()?.let { func.invoke(it) }
    }
}
