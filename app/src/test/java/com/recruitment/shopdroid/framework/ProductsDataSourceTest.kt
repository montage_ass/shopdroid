package com.recruitment.shopdroid.framework

import android.net.Uri
import androidx.paging.PagingSource
import com.recruitment.domain.Product
import com.recruitment.domain.ProductListResponse
import data.ProductRepository
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.unmockkAll
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import retrofit2.Response

@ExperimentalCoroutinesApi
class ProductsDataSourceTest {

    private val productList = listOf<Product>(
        mockk(),
        mockk()
    )

    private val repository = mockk<ProductRepository>()
    private val previousUri = mockk<Uri>()
    private val nextUri = mockk<Uri>()

    @Before
    fun setUp() {
        mockkStatic(Uri::class)
        every { Uri.parse("previous") } returns previousUri
        every { previousUri.getQueryParameter("page") } returns "1"
        every { Uri.parse("next") } returns nextUri
        every { nextUri.getQueryParameter("page") } returns "3"
    }

    @After
    fun tearDown() {
        unmockkAll()
    }

    @Test
    fun `test paging source successful loading of paging data`() = runBlockingTest {
        val pagingSource = ProductsDataSource(repository)
        val response = mockk<Response<ProductListResponse>>()
        val productListResponse = mockk<ProductListResponse>(relaxed = true)
        every { response.body() } returns productListResponse
        every { productListResponse.list } returns productList
        every { productListResponse._previous } returns "previous"
        every { productListResponse._next } returns "next"
        coEvery { repository.getProductList(any()) } returns response

        val loadResult = pagingSource.load(
            PagingSource.LoadParams.Refresh(
                key = null,
                loadSize = 3,
                placeholdersEnabled = false
            )
        )

        assertEquals(
            PagingSource.LoadResult.Page(
                data = productList,
                prevKey = 1,
                nextKey = 3
            ),
            loadResult
        )
    }

    @Test
    fun `test paging source error case when loading paging data`() = runBlockingTest {
        val pagingSource = ProductsDataSource(repository)
        coEvery { repository.getProductList(any()) } throws Exception()

        val loadResult = pagingSource.load(
            PagingSource.LoadParams.Refresh(
                key = null,
                loadSize = 3,
                placeholdersEnabled = false
            )
        )

        assertTrue(loadResult is PagingSource.LoadResult.Error)
    }
}