package com.recruitment.shopdroid.ui.productdetail

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.recruitment.domain.ProductDetailResponse
import com.recruitment.shopdroid.utils.MainCoroutineRule
import com.google.gson.JsonObject
import data.ProductRepository
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import okhttp3.ResponseBody
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Response

@ExperimentalCoroutinesApi
class ProductDetailViewModelTest {
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val coroutineRule = MainCoroutineRule()

    private val productRepository = mockk<ProductRepository>()
    private lateinit var viewModel: ProductDetailViewModel

    @Before
    fun setUp() {
        viewModel = ProductDetailViewModel(productRepository)
    }

    @Test
    fun `product details are mapped to display correctly and are emitted`() {
        val productDetailsResponse = mockk<Response<ProductDetailResponse>>()
        coEvery { productRepository.getProductDetails(any()) } returns productDetailsResponse

        val productDetails = ProductDetailResponse(
            23,
            "shorts",
            "nice shorts",
            "Adidas",
            45,
            "$",
            35,
            null,
            7
        )
        every { productDetailsResponse.isSuccessful } returns true
        every { productDetailsResponse.body() } returns productDetails

        val testObserver = mockk<Observer<ProductDetailDisplay>>(relaxed = true)
        viewModel.productDetail.observeForever(testObserver)

        viewModel.loadProductDetails(0)

        verify {
            testObserver.onChanged(
                ProductDetailDisplay(
                    "Shorts",
                    "nice shorts",
                    "Adidas",
                    "45.00 $",
                    "29.25 $",
                    true,
                    null,
                    true
                )
            )
        }
    }

    @Test
    fun `on error the user message is emitted`() {
        val productDetailsResponse = mockk<Response<ProductDetailResponse>>()
        coEvery { productRepository.getProductDetails(any()) } returns productDetailsResponse

        val errorJson = JsonObject().apply {
            addProperty("userMessage", "this is an error")
        }

        val errorBody = mockk<ResponseBody>()
        every { errorBody.string() } returns errorJson.toString()

        every { productDetailsResponse.isSuccessful } returns false
        every { productDetailsResponse.errorBody() } returns errorBody

        val testObserver = mockk<Observer<String?>>(relaxed = true)
        viewModel.error.observeForever(testObserver)

        viewModel.loadProductDetails(0)

        verify {
            testObserver.onChanged("this is an error")
        }
    }
}