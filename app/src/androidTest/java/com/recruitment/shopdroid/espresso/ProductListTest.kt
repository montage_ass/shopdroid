package com.recruitment.shopdroid.espresso

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.hasMinimumChildCount
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.recruitment.shopdroid.MainActivity
import com.recruitment.shopdroid.R
import com.recruitment.shopdroid.espresso.utils.waitUntilVisible
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ProductListTest {
    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun productListIsShownWithAtLeastOneItem() {
        onView(withId(R.id.product_list)).waitUntilVisible(1000)
        onView(withId(R.id.product_list)).check(matches(hasMinimumChildCount(1)))
    }

}
