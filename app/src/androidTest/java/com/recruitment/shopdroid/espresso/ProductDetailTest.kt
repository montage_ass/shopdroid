package com.recruitment.shopdroid.espresso

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.recruitment.shopdroid.MainActivity
import com.recruitment.shopdroid.R
import com.recruitment.shopdroid.espresso.utils.waitUntilVisible
import com.recruitment.shopdroid.ui.productlist.ProductListAdapter
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ProductDetailTest {
    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun productListIsShownWithAtLeastOneItem() {
        onView(withId(R.id.product_list)).waitUntilVisible(1000)
        onView(withId(R.id.product_list)).perform(
            RecyclerViewActions.actionOnItemAtPosition<ProductListAdapter.ProductListItemViewHolder>(
                0,
                click()
            )
        )

        onView(withId(R.id.product_image)).check(matches(isDisplayed()))
        onView(withId(R.id.brand)).check(matches(isDisplayed()))
        onView(withId(R.id.description)).check(matches(isDisplayed()))
        onView(withId(R.id.price_cart_layout)).check(matches(isDisplayed()))

        onView(withId(R.id.add_to_cart)).perform(click())

        onView(withText(R.string.coming_soon)).check(matches(isDisplayed()))
        onView(withText(android.R.string.ok)).perform(click())
    }

}